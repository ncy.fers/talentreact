import React, { Component } from 'react';
import Lista from './components/lista';
import './App.css';
import Footer from './components/footer';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: '',
      items: []
    };
  }

  onChange = (event) => {
    this.setState({ term: event.target.value });
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.setState({
      term: '',
      items: [...this.state.items, this.state.term]
    });
  }
  
  render() {
    return (
      <div className="App">
        <div className="container">
          <h1 className="display-4">Lista de Compras</h1>
          <form className="App-Form" 
                onSubmit={this.onSubmit}>
            <input 
              className="form-control" 
              aria-label="Exemplo do tamanho do input" 
              aria-describedby="inputGroup-sizing-sm"
              value={this.state.term} 
              onChange={this.onChange} 
            />
            <br />
            <button className="btn btn-primary">Adicionar Item</button>
            <br />
          </form>
          <div className="card-body">
            <Lista items={this.state.items} />
          </div>
        </div>
        <div className="container">
          <Footer />
        </div>
      </div>

    );
  }
}