import React from 'react';

const Lista = props => (
<div className="container-fluid">
           
  <ul className="list-group">
    {
      props.items.map((item, index) => <li className="list-group-item" key={index}>{item}</li>)
    }
  </ul>
  <br />
  </div>     

);

export default Lista;