import React, {Component} from 'react';
import {Icon} from 'antd';
import './footer.css';

export default class Footer extends Component {
  render() {
    return (
      <footer className="page-footer font-small blue fixed-bottom">
        <div className="mx-auto" >
          <div className="footer-copyright text-center py-3">
            © 2019 Copyright:
          </div>
          <a 
            href="mailto: ncy.fers@gmail.com"
            rel="noopener noreferrer"  
          >
              <Icon type="mail" />
          </a>
          <a
            href="https://www.instagram.com/dev.nfers/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Icon type="instagram" />
          </a>
          <a
            href="https://www.linkedin.com/in/nayara-ferreira-b6468673/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Icon type="linkedin" />
          </a>
        </div>
        <br />
      </footer>
    );
  }
}
