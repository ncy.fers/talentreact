import React from 'react';
import styled from 'styled-components';

const DivStyle  = styled.div`
    color: blue;
    background: url('$props => props.imageUrl}');
`;


function HelloWorldComponent() {
    const url = 'https://sourcerer.io/nfers';
    return <DivStyle imageUrl={url}>Hello denovo</DivStyle>;
};

export default HelloWorldComponent;